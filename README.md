# Da!

With da you can quickly change directories without having to alias so many "cd blah blahs".
All the directories are stored in a JSON file for convenience.
Example JSON file:
`'
{
  "cases": {
    "home": "~",
    "h_asd": "/home/asd/",
    "jdan": "/home/jdan/",
    "ubin": "/usr/bin",
    "kl": "/.kl/"
  }
}
'`
# Installation
Copy da.bash to convenient directory.
Modify at line 1: 
`in=/path/to/json`
Write a new line in the .bashrc file at your home directory as such:
`source /path/to/da.bash`

# Upcoming features
Will create an initializer and modifier, so the path to the JSON file will be automatically specified.
The initializer and modifier will automatically modify the .bashrc file.